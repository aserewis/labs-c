package pk.labs.LabC.animal1.internal;

import pk.labs.LabC.contracts.Animal;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class Malpa implements Animal{
    private PropertyChangeSupport pcs;
    private String status;

    @Override
    public String getSpecies() {
        return "Malpka";
    }

    @Override
    public String getName() {
        return "Ozzy";
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        String oldStatus = this.status;
        this.status = status;
        pcs.firePropertyChange("status", oldStatus, this.status);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }
}
