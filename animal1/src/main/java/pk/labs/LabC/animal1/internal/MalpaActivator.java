package pk.labs.LabC.animal1.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.logger.Logger;

public class MalpaActivator implements BundleActivator {
    @Override
    public void start(BundleContext bundleContext) throws Exception {
        bundleContext.registerService(Animal.class.getName(), new Malpa(), null);
        Logger.get().log(this, "Malpa je banana");
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        Logger.get().log(this, "Malpa śpi");
    }
}
