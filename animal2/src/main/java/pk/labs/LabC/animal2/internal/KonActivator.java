package pk.labs.LabC.animal2.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.logger.Logger;

public class KonActivator implements BundleActivator {
    @Override
    public void start(BundleContext bundleContext) throws Exception {
        bundleContext.registerService(Animal.class.getName(), new Kon(), null);
        Logger.get().log(this, "Kon skacze");
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        Logger.get().log(this, "Zwierze w stajni");
    }
}
