package pk.labs.LabC.animal3.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.logger.Logger;

public class PapugaActivator implements BundleActivator {
    @Override
    public void start(BundleContext bundleContext) throws Exception {
        bundleContext.registerService(Animal.class.getName(), new Papuga(), null);
        Logger.get().log(this, "Gada do ludzi!");
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        Logger.get().log(this, "Ptak je karme");
    }
}
